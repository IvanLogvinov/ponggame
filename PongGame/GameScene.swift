//
//  GameScene.swift
//  PongGame
//
//  Created by Ivan Logvinov on 10/23/16.
//  Copyright © 2016 Ivan Logvinov. All rights reserved.
//

import SpriteKit
import GameplayKit

@objc protocol GameOverDelegate {
    func gameOverDelegateFunc()
}

private var vectorX = 10;
private var vectorY = 10;
private var numberOfVictories = 10;


class GameScene: SKScene {
    
    var gamescene_delegate : GameOverDelegate?

    
    var ball = SKSpriteNode()
    var enemy = SKSpriteNode()
    var main = SKSpriteNode()
    
    var btmLabel = SKLabelNode()
    var topLabel = SKLabelNode()
    
    var player1 = SKLabelNode()
    var player2 = SKLabelNode()
    
    var exit = SKLabelNode()
    
    var score = [Int]()
    var isRetryCheck = false;
    
    
    
    override func didMove(to view: SKView) {

        ball = self.childNode(withName: "ball") as! SKSpriteNode
        let trailNode = SKNode()
        trailNode.zPosition = 1
        addChild(trailNode)
        let trail = SKEmitterNode(fileNamed: "BallTrail")!
        trail.targetNode = trailNode
        
        ball.addChild(trail)
        
        btmLabel = self.childNode(withName: "btmLabel") as! SKLabelNode
        topLabel = self.childNode(withName: "topLabel") as! SKLabelNode
        exit = self.childNode(withName: "exit") as! SKLabelNode
        
        player1 = self.childNode(withName: "player1") as! SKLabelNode
        player2 = self.childNode(withName: "player2") as! SKLabelNode

        
        enemy = self.childNode(withName: "enemy") as! SKSpriteNode
        enemy.position.y = (self.frame.height / 2) - 50
        
        main = self.childNode(withName: "main") as! SKSpriteNode
        main.position.y = (-self.frame.height / 2) + 50

        let border = SKPhysicsBody(edgeLoopFrom: self.frame)

        border.friction = 0
        border.restitution = 1
        
        self.physicsBody = border
        
        startGame()
        
    }
    
    func startGame() {
        MusicHelper.sharedHelper.playBackgroundMusic()
        scene?.view?.isPaused = false
        isRetryCheck = false
        exit.text = "Pause"
        score = [0,0]
        topLabel.text = "\(score[1])"
        btmLabel.text = "\(score[0])"
        
        if currentGameType == .player2 {
            player1.isHidden = false;
            player2.isHidden = false;
        } else {
            player1.isHidden = true;
            player2.isHidden = true;
        }
        
        ball.physicsBody?.applyImpulse(CGVector(dx: 10, dy: 10))
    }
    
    func stopGame() {
        scene?.view?.isPaused = true
        isRetryCheck = true
        score = [Int]()
    }
    
    func addScore(playerWhoWon : SKSpriteNode) {
        
        if score[0]==numberOfVictories {
            exit.text = "You win, Retry?"
            stopGame()

        } else if (score[1]==numberOfVictories) {
            exit.text = "You lose, Retry?"
            stopGame()
            
            
        } else {
            
            ball.position = CGPoint(x: 0, y: 0)
            // vectorX += 20;
            // vectorY += 20;
            // print("speed: ",vectorX, vectorY)
            ball.physicsBody?.velocity = CGVector(dx: 10, dy: 10)
            
            if playerWhoWon == main {
                score[0] += 1
                ball.physicsBody?.applyImpulse(CGVector(dx: 10, dy: 10))

            } else if playerWhoWon == enemy {
                score[1] += 1
                ball.physicsBody?.applyImpulse(CGVector(dx: -10, dy: -10))

            }
                //print(score)
                topLabel.text = "\(score[1])"
                btmLabel.text = "\(score[0])"
            }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for touch in touches {
            let location = touch.location(in: self)
            let touchedNode = self.atPoint(location)
            
            if let name = touchedNode.name
            {
                if name == "exit"
                {
                    if isRetryCheck {
                        startGame()
                        

                    } else {
                        if (scene?.view?.isPaused)! {
                            exit.text = "Pause"
                            scene?.view?.isPaused = false
                            print("Unpaused")
                            
                        } else {
                            exit.text = "Paused"
                            scene?.view?.isPaused = true
                            print("Paused")
                            
                        }
                    }
                }
            }
            
            if currentGameType == .player2 {
                if location.y > 0 {
                    enemy.run(SKAction.moveTo(x: location.x, duration: 0.2))
                }
                
                if location.y < 0 {
                    main.run(SKAction.moveTo(x: location.x, duration: 0.2))
                }
            }
            
            else {
                main.run(SKAction.moveTo(x: location.x, duration: 0.2))
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            
            if currentGameType == .player2 {
                if location.y > 0 {
                    enemy.run(SKAction.moveTo(x: location.x, duration: 0.2))
                }
                
                if location.y < 0 {
                    main.run(SKAction.moveTo(x: location.x, duration: 0.2))
                }
            }
            
            else {
                main.run(SKAction.moveTo(x: location.x, duration: 0.2))
            }
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        
        switch currentGameType {
        case .easy:
            enemy.run(SKAction.moveTo(x: ball.position.x, duration: 1.0))

            break
        case .medium:
            enemy.run(SKAction.moveTo(x: ball.position.x, duration: 0.8))

            break
        case .hard:
            enemy.run(SKAction.moveTo(x: ball.position.x, duration: 0.2))

            break
            
        case .player2:
            
            break
        }
        
        
        
     //   enemy.run(SKAction.moveTo(x: ball.position.x, duration: 1.0))
        
        //position of the ball which recognize losing
        if ball.position.y <= main.position.y - 30 {
            addScore(playerWhoWon: enemy)
        } else if ball.position.y >= enemy.position.y + 30 {
            addScore(playerWhoWon:main)
        }
    }
}
