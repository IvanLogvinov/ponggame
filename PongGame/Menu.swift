//
//  Menu.swift
//  PongGame
//
//  Created by Ivan Logvinov on 1/28/17.
//  Copyright © 2017 Ivan Logvinov. All rights reserved.
//

import Foundation
import UIKit
import GoogleMobileAds

enum gameType {
    case easy
    case medium
    case hard
    case player2
}


class Menu: UIViewController {

    @IBOutlet weak var bannerView: GADBannerView!
    
    override func viewDidLoad() {
        MusicHelper.sharedHelper.playBackgroundMusic()
        bannerView.adUnitID = "ca-app-pub-7776997754462703/1022200275"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
    }
    
    @IBAction func Players(_ sender: Any) {
        moveToGame(game: .player2)
    }
    
    @IBAction func Easy(_ sender: Any) {
        moveToGame(game: .easy)
    }
    
    @IBAction func Medium(_ sender: Any) {
        moveToGame(game: .medium)
    }
    
    @IBAction func Hard(_ sender: Any) {
        moveToGame(game: .hard)
    }
 
    func moveToGame(game : gameType) {
        let gameVC = self.storyboard?.instantiateViewController(withIdentifier: "gameVC") as! GameViewController
        
        currentGameType = game
        
        //self.performSegue(withIdentifier: "playGame", sender: nil)
        
        self.navigationController?.pushViewController(gameVC, animated: true)
    }
}
